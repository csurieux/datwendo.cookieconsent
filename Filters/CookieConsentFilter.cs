﻿using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Datwendo.CookieConsent.Models;
using Orchard;
using Orchard.ContentManagement;
using Orchard.DisplayManagement;
using Orchard.Localization;
using Orchard.Mvc.Filters;
using Orchard.UI.Admin;
using Orchard.UI.Admin.Notification;
using Orchard.UI.Notify;

namespace Datwendo.CookieConsent.Filters {
    public class CookieConsentFilter : FilterProvider, IResultFilter {

    private readonly INotificationManager _notificationManager;
    private readonly IWorkContextAccessor _workContextAccessor;
    private readonly dynamic _shapeFactory;
    public Localizer T { get; set; }

        public CookieConsentFilter(INotificationManager notificationManager,
                                IWorkContextAccessor workContextAccessor,
                                IShapeFactory shapeFactory) {
        _notificationManager = notificationManager;
        _workContextAccessor = workContextAccessor;
        _shapeFactory = shapeFactory;
        T = NullLocalizer.Instance;
        }

        public void OnResultExecuting(ResultExecutingContext filterContext) {
            if ( AdminFilter.IsApplied(filterContext.RequestContext))
                return;

            // if it's not a view result, a redirect for example
            if (!(filterContext.Result is ViewResultBase))
                return;

            // if it's a child action, a partial view for example
            if (filterContext.IsChildAction)
                return;

            var user = filterContext.HttpContext.User;
            if (user != null && user.Identity.IsAuthenticated)
                return;

            var request = filterContext.HttpContext.Request;

            var settingsPart = _workContextAccessor.GetContext().CurrentSite.As<CookieConsentSettingsPart>();

            HttpCookieCollection cookies = request.Cookies;
            for (int i = 0; i < cookies.Count; i++) {
                var coo = cookies[i];
                var name = coo.Name;
                if (name.Contains(settingsPart.CookieName))
                    return; 
            }
            
            if (string.IsNullOrEmpty(settingsPart.Text))
                return;

            // Insert a shape built with settinspart content in the Message Zone 
            if (!string.IsNullOrEmpty(settingsPart.Text)) {
                var messagesZone = _workContextAccessor.GetContext(filterContext).Layout.Zones["Messages"];
                messagesZone = messagesZone.Add(_shapeFactory.CookieConsent(CookieName: settingsPart.CookieName, 
                    HeaderText: settingsPart.HeaderText,
                    ButtonText : settingsPart.ButtonText,
                    Delay: settingsPart.CookieExpireDelay, 
                    Text: settingsPart.Text, HtmlText: new HtmlString(settingsPart.Text)));
            }
        }

        public void OnResultExecuted(ResultExecutedContext filterContext) { }

    }
}