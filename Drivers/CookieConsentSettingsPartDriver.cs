﻿using Datwendo.CookieConsent.Models;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.Handlers;
using Orchard.Localization;

namespace Datwendo.CookieConsent.Drivers {
    public class CookieConsentSettingsPartDriver : ContentPartDriver<CookieConsentSettingsPart> {
        private const string TemplateName = "Parts/CookieConsentSettings";
        public CookieConsentSettingsPartDriver() {
            T = NullLocalizer.Instance;
        }

        public Localizer T { get; set; }

        protected override string Prefix {
            get { return "CookieConsentSettings"; }
        }

        protected override DriverResult Editor(CookieConsentSettingsPart part, dynamic shapeHelper) {
            return ContentShape("Parts_CookieConsentSettings_Edit",
                () => shapeHelper.EditorTemplate(TemplateName: TemplateName, Model: part, Prefix: Prefix))
                .OnGroup("CookieConsent");
        }

        protected override DriverResult Editor(CookieConsentSettingsPart part, IUpdateModel updater, dynamic shapeHelper) {
            if (updater.TryUpdateModel(part, Prefix, null, null)) {

            }
            return Editor(part, shapeHelper);
        }

        protected override void Importing(CookieConsentSettingsPart part, ImportContentContext context) {
            var elementName = part.PartDefinition.Name;
        }

        protected override void Exporting(CookieConsentSettingsPart part, ExportContentContext context) {
            var el = context.Element(part.PartDefinition.Name);
        }
    }
}