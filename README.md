# Datwendo.CookieConsent is an Orchard 1.9.x module which displays the legal European warning concerning cookies on first access #

* Adapt the corresponding template in your theme
* Version 1.0


* Christian Surieux - Datwendo - 2016
* Apache v2 license