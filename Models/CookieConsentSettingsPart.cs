﻿using Orchard.ContentManagement;
using Orchard.ContentManagement.FieldStorage.InfosetStorage;

namespace Datwendo.CookieConsent.Models {

    public class CookieConsentSettingsPart : ContentPart {
        public string CookieName {
            get {
                var nm = this.As<InfosetPart>().Get<CookieConsentSettingsPart>("CookieName");
                return nm ?? "Consent";
            }
            set {
                this.As<InfosetPart>().Set<CookieConsentSettingsPart>("CookieName", value);
            }
        }

        public int CookieExpireDelay {
            get {
                string days = this.As<InfosetPart>().Get<CookieConsentSettingsPart>("CookieExpireDelay");
                int nb = 0;
                if (int.TryParse(days, out nb))
                    return nb;
                return 30;
            }
            set {
                this.As<InfosetPart>().Set<CookieConsentSettingsPart>("CookieExpireDelay", value.ToString());
            }
        }

        public string ButtonText {
            get {
                var nm = this.As<InfosetPart>().Get<CookieConsentSettingsPart>("ButtonText");
                return nm ?? "Accept";
            }
            set {
                this.As<InfosetPart>().Set<CookieConsentSettingsPart>("ButtonText", value);
            }
        }

        public string HeaderText {
            get {
                return this.As<InfosetPart>().Get<CookieConsentSettingsPart>("HeaderText");
            }
            set {
                this.As<InfosetPart>().Set<CookieConsentSettingsPart>("HeaderText", value);
            }
        }

        public string Text {
            get {
                return this.As<InfosetPart>().Get<CookieConsentSettingsPart>("Text");
            }
            set {
                this.As<InfosetPart>().Set<CookieConsentSettingsPart>("Text", value);
            }
        }
    }
}
