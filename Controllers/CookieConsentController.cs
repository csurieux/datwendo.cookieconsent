﻿using System;
using System.Web.Mvc;

namespace Datwendo.CookieConsent.Controllers {

    public class CookieConsentController : Controller {
        public CookieConsentController() {
        }

        [HttpPost]
        public JsonResult Accept(string referrer) {
            return new JsonResult { Data = string.Format("Ok-{0:u}",DateTime.Now) };
        }
    }
}