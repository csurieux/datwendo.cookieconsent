﻿using Datwendo.CookieConsent.Models;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Handlers;
using Orchard.Localization;

namespace Datwendo.CookieConsent.Handlers {
    public class CookieConsentSettingsPartHandler : ContentHandler {
        public CookieConsentSettingsPartHandler() {
            T = NullLocalizer.Instance;
            Filters.Add(new ActivatingFilter<CookieConsentSettingsPart>("Site"));
        }

        public Localizer T { get; set; }

        protected override void GetItemMetadata(GetContentItemMetadataContext context) {
            if (context.ContentItem.ContentType != "Site")
                return;
            base.GetItemMetadata(context);
            context.Metadata.EditorGroupInfo.Add(new GroupInfo(T("Cookie Consent")) {
                Id = "CookieConsent",
                Position = "4"
            });
        }
    }
}
