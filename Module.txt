﻿Name: Datwendo.CookieConsent
AntiForgery: enabled
Author: C. Surieux
Website: https://www.datwendo.co
Version: 1.0
OrchardVersion: 1.9
Description: Do the necessary European 'Cookies Directive' job
Features:
    Datwendo.CookieConsent:
        Description: Display a content to collect cookie consent
		Name: Datwendo Cookie Consent
		Category: Law